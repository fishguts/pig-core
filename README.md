# Pig: Core Library

## Overview

Includes base level functionality. For the most part it does not depend on `NodeJS` but where it does we attempt to abtract the implementation using an `interface` approach.
