/**
 * User: curtis
 * Date: 3/1/18
 * Time: 9:42 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("../../lib/assert");
const proxy=require("../../lib/proxy");


describe("assert", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("immutable", function() {
		it("should throw an exception if object is changed", function() {
			const o1={}, o2={},
				assertImmutable=assert.immutable(o1, o2);
			o2.a=1;
			assert.throws(assertImmutable);
		});

		it("should not throw an exception if object is unchanged", function() {
			const o1={},
				assertImmutable=assert.immutable(o1);
			assertImmutable();
			assert.doesNotThrow(assertImmutable);
		});
	});

	describe("isError", function() {
		it("should not throw exception if param is an error", function() {
			assert.doesNotThrow(()=>assert.isError(new Error()));
		});

		it("should throw exception if param is not in error", function() {
			assert.throws(()=>assert.isError());
		});

		it("should not throw exception if async and callback param is an error", function(done) {
			assert.isError(()=>{
				done();
			})(new Error());
		});

		it("should throw exception if async and callback param is not an error", function(done) {
			try {
				assert.isError(()=>{
					done();
				})();
			} catch(error) {
				assert.ok(error instanceof Error);
				done();
			}
		});
	});

	describe("isNotError", function() {
		it("should not throw exception if param is an error", function() {
			assert.doesNotThrow(()=>assert.isNotError());
		});

		it("should throw exception if param is not in error", function() {
			assert.throws(()=>assert.isNotError(new Error()));
		});

		it("should not throw exception if async and callback param is an error", function(done) {
			assert.isNotError(()=>{
				done();
			})();
		});

		it("should throw exception if async and callback param is not an error", function(done) {
			try {
				assert.isNotError(()=>{
					done();
				})(new Error());
			} catch(error) {
				assert.ok(error instanceof Error);
				done();
			}
		});
	});

	describe("toLog", function() {
		it("should not assert if condition is true", function() {
			assert.toLog(true);
			assert.equal(proxy.log.error.callCount, 0);
		});

		it("should assert if condition is false", function() {
			assert.toLog(false);
			assert.equal(proxy.log.error.callCount, 1);
		});
	});
});
