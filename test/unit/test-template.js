/**
 * User: curtis
 * Date: 6/4/18
 * Time: 10:56 AM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("../../lib/assert");
const template=require("../../lib/template");

describe("template", function() {
	it("should properly substitute variables in properly marked up template string", function() {
		const _template="This is a {{noun}} of the {{adjective}} broadcast system";
		assert.equal(template.render(_template, {
			adjective: "emergency",
			noun: "test"
		}), "This is a test of the emergency broadcast system");
		assert.equal(template.render(_template, {
			adjective: "fuzzy",
			noun: "cat"
		}), "This is a cat of the fuzzy broadcast system");
	});

	it("should properly substitute side by side variables", function() {
		const _template="{{a}}{{b}}";
		assert.equal(template.render(_template, {
			a: "one",
			b: "two"
		}), "onetwo");
	});
});
