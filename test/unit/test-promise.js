/**
 * User: curtis
 * Date: 2/27/18
 * Time: 9:26 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("../../lib/assert");
const promise=require("../../lib/promise");
const proxy=require("../../lib/proxy");

describe("promise", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("series", function() {
		it("should throw an error if the elements don't look like factories", function() {
			assert.throws(()=>{
				promise.series([new Promise(_.noop)]);
			});
		});

		it("should properly create and return a promise chain", function(done) {
			const accumulator=[];
			promise.series([
				()=>new Promise((resolve)=>{
					accumulator.push(1);
					resolve();
				}),
				()=>new Promise((resolve)=>{
					accumulator.push(2);
					resolve();
				}),
				()=>new Promise((resolve)=>{
					accumulator.push(3);
					resolve();
				})
			]).then(()=>{
				assert.deepEqual(accumulator, [1, 2, 3]);
				done();
			});
		});
	});
});
