/**
* User: curt
* Date: 05/27/18
* Time: 1:08 AM
*/

const assert=require("assert");
const {StringFilter}=require("../../lib/filter");

describe("filter", function() {
	describe("StringFilter", function() {
		it("should not filter an instance with no filters", function() {
			const filter=new StringFilter();
			assert.equal(filter.apply("test"), "test");
			assert.deepEqual(filter.apply(["one", "two"]), ["one", "two"]);
		});

		it("should properly filter on 'include'", function() {
			const filter=new StringFilter({
				include: ["test"]
			});
			assert.equal(filter.apply("test"), "test");
			assert.deepEqual(filter.apply(["test", "fail"]), ["test"]);
		});

		it("should properly filter on 'ignore", function() {
			const filter=new StringFilter({
				ignore: ["fail"]
			});
			assert.equal(filter.apply("test"), "test");
			assert.deepEqual(filter.apply(["test", "fail"]), ["test"]);
		});
	});
});
