/**
 * User: curtis
 * Date: 11/3/18
 * Time: 12:36 AM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const sinon=require("sinon");
const notification=require("../../lib/notification");

describe("notification", function() {
	it("should add listeners, emit and remove listeners", function() {
		const stub=sinon.stub();
		notification.addListener("test", stub);
		notification.emit("test");
		assert.equal(stub.callCount, 1);
		notification.removeListener("test", stub);
		notification.emit("test");
		assert.equal(stub.callCount, 1);
	});

	it("emit should pass parameter if included", function() {
		const stub=sinon.stub();
		notification.addListener("test", stub);
		notification.emit("test", "param");
		assert.deepEqual(stub.getCall(0).args, ["param"]);
	});
});
