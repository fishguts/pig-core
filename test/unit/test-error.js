/**
 * User: curt
 * Date: 05/27/18
 * Time: 7:15 PM
 */

const _=require("lodash");
const assert=require("../../lib/assert");
const {PigError}=require("../../lib/error");

describe("error", function() {
	describe("PigError", function() {
		function _toPOJO(error) {
			return _(error)
				.pick(Object.getOwnPropertyNames(error))
				.omit(["stack"])
				.value();
		}

		describe("constructor", function() {
			it("should use message if the only param is text", function() {
				const instance=new PigError({
					message: "message"
				});
				assert.deepEqual(_toPOJO(instance), {
					message: "message"
				});
			});

			it("should populate other known properties properly", function() {
				const instance=new PigError({
					message: "message",
					details: "details",
					statusCode: 100
				});
				assert.deepEqual(_toPOJO(instance), {
					message: "message",
					details: "details",
					statusCode: 100
				});
			});

			it("should pull in details from an error", function() {
				const error=new Error("message"),
					instance=new PigError({error});
				assert.deepEqual(_toPOJO(instance), {
					message: "message",
					error: error
				});
			});

			it("make details of fallback params", function() {
				assert.deepEqual(_toPOJO(new PigError({
					message: "message",
					error: new Error("details")
				})), {
					message: "message",
					details: "details",
					error: new Error("details")
				});
				assert.deepEqual(_toPOJO(new PigError({
					message: "message",
					statusCode: 100
				})), {
					message: "message",
					details: "Continue (100)",
					statusCode: 100
				});
			});

			it("annotate 'instance' data", function() {
				class Test {
					toString() {return "test";}
				}
				const instance=new PigError({
					message: "message",
					instance: new Test()
				});
				assert.deepEqual(_toPOJO(instance), {
					message: "message",
					instance: {
						type: "Test"
					}
				});
			});

			it("should inherit the param's error's stack", function() {
				const error=new Error();
				error.stack="stack";
				const instance=new PigError({error});
				assert.equal(instance.stack, "stack");
			});
		});
	});
});
