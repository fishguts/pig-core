/**
 * User: curtis
 * Date: 11/1/18
 * Time: 12:21 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("../../lib/assert");
const index=require("../../lib/index");

describe("index", function() {
	it("should properly import each public module", function() {
		const set=new Set(),
			moduleNames=[
				"assert",
				"configuration",
				"constant",
				"date",
				"diagnostics",
				"error",
				"file",
				"filter",
				"format",
				"http",
				"introspect",
				"json",
				"log",
				"mutation",
				"notification",
				"promise",
				"proxy",
				"template",
				"urn",
				"util",
				"validator"
			];
		moduleNames.forEach((name)=>{
			const moduleLoad=_.get(index, name),
				moduleCache=_.get(index, name);
			assert.equal(typeof(moduleLoad), "object", `module=${name} failed`);
			assert.strictEqual(moduleLoad, moduleCache);
			set.add(moduleLoad);
		});
		assert.equal(moduleNames.length, set.size);
	});
});
