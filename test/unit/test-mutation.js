/**
 * User: curtis
 * Date: 3/9/18
 * Time: 9:49 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("../../lib/assert");
const mutable=require("../../lib/mutation").mutable;
const immutable=require("../../lib/mutation").immutable;


describe("mutation", function() {
	describe("mutable", function() {
		describe("array", function() {
			describe("add", function() {
				it("should create an array if param is null", function() {
					assert.deepEqual(mutable.array.add(null, 1), [1]);
				});

				it("should append an element to an existing array", function() {
					const original=[1];
					mutable.array.add(original, 2);
					assert.deepEqual(original, [1, 2]);
				});

				it("should insert an element to an existing array", function() {
					const original=[1, 3];
					mutable.array.add(original, 2, 1);
					assert.deepEqual(original, [1, 2, 3]);
				});
			});

			describe("concat", function() {
				it("should return original if param is null", function() {
					assert.deepEqual(mutable.array.concat(null, [1]), [1]);
				});

				it("should append an element to an existing array", function() {
					const original=[1];
					mutable.array.concat(original, [2, 3]);
					assert.deepEqual(original, [1, 2, 3]);
				});

				it("should insert elements into an existing array", function() {
					const original=[1, 4];
					mutable.array.concat(original, [2, 3], 1);
					assert.deepEqual(original, [1, 2, 3, 4]);
				});
			});

			describe("remove", function() {
				it("should remove an element by reference", function() {
					const original=["good", "bad"];
					mutable.array.remove(original, {element: "bad"});
					assert.deepEqual(original, ["good"]);
				});

				it("should remove an element by index", function() {
					const original=["good", "bad"];
					mutable.array.remove(original, {index: 0});
					assert.deepEqual(original, ["bad"]);
				});
			});

			describe("replace", function() {
				it("should replace an element by reference", function() {
					const original=["good", "bad"];
					mutable.array.replace(original, "new", {element: "bad"});
					assert.deepEqual(original, ["good", "new"]);
				});

				it("should replace an element by index", function() {
					const original=["good", "bad"];
					mutable.array.replace(original, "new", {index: 1});
					assert.deepEqual(original, ["good", "new"]);
				});
			});
		});
	});

	describe("immutable", function() {
		describe("array", function() {
			describe("add", function() {
				it("should create an array if param is null", function() {
					assert.deepEqual(immutable.array.add(null, 1), [1]);
				});

				it("should append an element to an existing array", function() {
					const original=[1],
						assertImmutable=assert.immutable(original);
					assert.deepEqual(immutable.array.add(original, 2), [1, 2]);
					assert.doesNotThrow(assertImmutable);
				});

				it("should insert an element to an existing array", function() {
					const original=[1, 3],
						assertImmutable=assert.immutable(original);
					assert.deepEqual(immutable.array.add(original, 2, 1), [1, 2, 3]);
					assert.doesNotThrow(assertImmutable);
				});
			});

			describe("concat", function() {
				it("should return original if param is null", function() {
					assert.deepEqual(immutable.array.concat(null, [1]), [1]);
				});

				it("should append an element to an existing array", function() {
					const original=[1],
						assertImmutable=assert.immutable(original);
					assert.deepEqual(immutable.array.concat(original, [2, 3]), [1, 2, 3]);
					assert.doesNotThrow(assertImmutable);
				});

				it("should insert elements into an existing array", function() {
					const original=[1, 4],
						assertImmutable=assert.immutable(original);
					assert.deepEqual(immutable.array.concat(original, [2, 3], 1), [1, 2, 3, 4]);
					assert.doesNotThrow(assertImmutable);
				});
			});

			describe("remove", function() {
				it("should remove an element by reference", function() {
					const original=["good", "bad"],
						assertImmutable=assert.immutable(original);
					assert.deepEqual(immutable.array.remove(original, {element: "bad"}), ["good"]);
					assert.doesNotThrow(assertImmutable);
				});

				it("should remove an element by index", function() {
					const original=["good", "bad"],
						assertImmutable=assert.immutable(original);
					assert.deepEqual(immutable.array.remove(original, {index: 0}), ["bad"]);
					assert.doesNotThrow(assertImmutable);
				});
			});

			describe("replace", function() {
				it("should replace an element by reference", function() {
					const original=["good", "bad"],
						assertImmutable=assert.immutable(original);
					assert.deepEqual(immutable.array.replace(original, "new", {element: "bad"}), ["good", "new"]);
					assert.doesNotThrow(assertImmutable);
				});

				it("should replace an element by index", function() {
					const original=["good", "bad"],
						assertImmutable=assert.immutable(original);
					assert.deepEqual(immutable.array.replace(original, "new", {index: 1}), ["good", "new"]);
					assert.doesNotThrow(assertImmutable);
				});
			});
		});
	});
});
