/* eslint-disable no-console */
/**
 * User: curtis
 * Date: 2018-12-08
 * Time: 17:16
 * Copyright @2018 by Xraymen Inc.
 */

const log=require("../../../lib/log/winston");

describe("log.winston", function() {
	describe("configuration", function() {
		it("should properly configure the logger", function() {
			// let's just make sure he doesn't blow up
			log.configure({
				applicationName: "name",
				configuration: {},
				environment: "env"
			});
		});
	});
});
