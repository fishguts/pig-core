/* eslint-disable no-console */
/**
 * User: curtis
 * Date: 2018-12-08
 * Time: 17:16
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const log=require("../../../lib/log/console");

describe("log.console", function() {
	describe("configuration", function() {
		it("should properly configure the logger", function() {
			// let's just make sure he doesn't blow up
			log.configure({
				applicationName: "name",
				configuration: {},
				environment: "env"
			});
		});
	});

	describe("level", function() {
		it("should properly get/set a level", function() {
			log.level.set("warn");
			assert.strictEqual(log.level.get(), "warn");
			assert.strictEqual(log.level.isLog("debug"), false);
			assert.strictEqual(log.level.isLog("warn"), true);
		});
	});
});
