/**
 * User: curtis
 * Date: 12/8/17
 * Time: 8:05 PM
 * Copyright @2017 by Xraymen Inc.
 */

const assert=require("assert");
const constant=require("../../lib/constant");

describe("common.constant", function() {
	it("should validate application mode values properly", function() {
		assert.equal(constant.isValidApplicationMode(constant.application.mode.CLI), true);
		assert.equal(constant.isValidApplicationMode("unknown"), false);
	});

	it("should validate event values properly", function() {
		assert.equal(constant.isValidEvent(constant.event.SETTINGS_LOADED), true);
		assert.equal(constant.isValidEvent("unknown"), false);
	});

	it("should validate priority values properly", function() {
		assert.equal(constant.isValidPriority(constant.priority.HIGH), true);
		assert.equal(constant.isValidPriority("unknown"), false);
	});

	it("should validate severity values properly", function() {
		assert.equal(constant.isValidSeverity(constant.severity.DEBUG), true);
		assert.equal(constant.isValidSeverity("unknown"), false);
	});

	describe("severity", function() {
		describe("trips", function() {
			it("should properly evaluate values", function() {
				assert.strictEqual(constant.severity.trips(constant.severity.DEBUG), false);
				assert.strictEqual(constant.severity.trips(constant.severity.INFO), false);
				assert.strictEqual(constant.severity.trips(constant.severity.WARN), true);
				assert.strictEqual(constant.severity.trips(constant.severity.ERROR), true);
				assert.strictEqual(constant.severity.trips(constant.severity.FATAL), true);
			});
		});
	});
});
