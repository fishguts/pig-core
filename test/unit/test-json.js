/**
 * User: curtis
 * Date: 12/2/17
 * Time: 8:31 PM
 * Copyright @2017 by Xraymen Inc.
 */

const assert=require("../../lib/assert");
const json=require("../../lib/json");


describe("codec.json", function() {
	describe("parse", function() {
		it("should parse and return directly if no callback", function() {
			const test={test: "data"};
			assert.deepEqual(json.parse(JSON.stringify(test)), test);
		});

		it("should parse and return via callback if included", function(done) {
			const test={test: "data"};
			json.parse(JSON.stringify(test), function(error, result) {
				assert.deepEqual(result, test);
				done();
			});
		});

		it("should throw exception if parse fails and no callback", function() {
			assert.throws(json.parse.bind(null, "bad-json"));
		});

		it("should callback with error if parse fails", function() {
			assert.isError(json.parse.bind(null, "bad-json"));
		});
	});
});
