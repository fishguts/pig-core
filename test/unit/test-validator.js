/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:35 AM
 * Copyright @2018 by Xraymen Inc.
 */

const fs=require("fs-extra");
const assert=require("../../lib/assert");
const validator=require("../../lib/validator");
const {PigError}=require("../../lib/error");


describe("validator", function() {
	describe("addSchema", function() {
		it("should accept a path", function() {
			const libraryPath="./test/data/schema-valid.json";
			if(fs.pathExistsSync(libraryPath)) {
				validator.addSchema(libraryPath);
			}
		});
	});

	describe("validateData", function() {
		it("should pass valid data", function() {
			const schema={
					type: "object",
					properties: {
						"name": {type: "string"}
					}
				},
				data={name: "Bob"};
			assert.equal(validator.validateData(data, schema), null);
		});

		it("should throw an exception on invalid data", function() {
			const schema={
					type: "object",
					properties: {
						"name": {type: "string"}
					}
				},
				data={name: new Date()};
			assert.throws(()=>validator.validateData(data, schema), function(error) {
				return error instanceof PigError;
			});
		});

		it("should return an error if throw is overriden", function() {
			const schema={
					type: "object",
					properties: {
						"name": {type: "string"}
					}
				},
				data={name: new Date()},
				result=validator.validateData(data, schema, {
					throw: false
				});
			assert.ok(result instanceof PigError);
		});
	});
});
