/**
 * User: curt
 * Date: 05/27/18
 * Time: 1:08 AM
 */

const _=require("lodash");
const assert=require("assert");
const fs=require("fs-extra");
const file=require("../../lib/file");
const os=require("os");

describe("file", function() {
	describe("relativePathToAbsolute", function() {
		it("should properly create an absolute path from a relative path", function() {
			const result=file.relativePathToAbsolute({
				module: module,
				relative: "./test-assert.js"
			});
			assert.ok(_.endsWith(result, "test/unit/test-assert.js"));
			assert.ok(fs.pathExistsSync(result));
		});

		it("should properly an absolute path from a path relative to the root", function() {
			const result=file.relativePathToAbsolute({
				module: module,
				relativeToModuleRoot: "test/unit/test-assert.js"
			});
			assert.ok(_.endsWith(result, "test/unit/test-assert.js"));
			assert.ok(fs.pathExistsSync(result));
		});
	});

	describe("readToJSON", function() {
		it("should properly read a json file", function(done) {
			file.readToJSON("./test/data/schema-valid.json", {}, (error, result)=>{
				assert.ifError(error);
				assert.ok(_.isPlainObject(result));
				done();
			});
		});
	});

	describe("readToJSONSync", function() {
		it("should properly read a json file", function() {
			const result=file.readToJSONSync("./test/data/schema-valid.json");
			assert.ok(_.isPlainObject(result));
		});
	});

	describe("writeJSON.sync", function() {
		it("should fail if path does not exist and not told to create", function() {
			assert.throws(()=>{
				file.writeJSON(`${os.tmpdir()}/${_.uniqueId()}/test.json`, {}, {
					async: false,
					createPath: false
				});
			});
		});

		it("should succeed if path does not exist but told to create", function() {
			file.writeJSON(`${os.tmpdir()}/${_.uniqueId()}/test.json`, {}, {
				async: false,
				createPath: true
			});
		});
	});

	describe("writeFile.async", function() {
		it("should fail if path does not exist and not told to create", function() {
			return file.writeFile(`${os.tmpdir()}/${_.uniqueId()}/test.json`, {}, {
				async: true,
				createPath: false
			})
				.then(()=>{
					assert.ok(false);
				})
				.catch((error)=>{
					assert.ok(error);
				});
		});

		it("should succeed if path does not exist but told to create", function() {
			return file.writeFile(`${os.tmpdir()}/${_.uniqueId()}/test.json`, {}, {
				async: true,
				createPath: true
			})
				.catch((error)=>{
					assert.ok(false);
				});
		});
	});

	describe("writeFile.sync", function() {
		it("should fail if path does not exist and not told to create", function() {
			assert.throws(()=>{
				file.writeFile(`${os.tmpdir()}/${_.uniqueId()}/test.json`, {}, {
					async: false,
					createPath: false
				});
			});
		});

		it("should succeed if path does not exist but told to create", function() {
			file.writeFile(`${os.tmpdir()}/${_.uniqueId()}/test.json`, {}, {
				async: false,
				createPath: true
			});
		});
	});
});
