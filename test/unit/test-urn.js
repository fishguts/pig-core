/**
 * User: curt
 * Date: 12/13/2017
 * Time: 5:33 PM
 */

const assert=require("../../lib/assert");
const urn=require("../../lib/urn");

describe("urn", function() {
	describe("create", function() {
		it("should properly create a urn", function() {
			assert.equal(urn.create("type", "name"), "urn:type:name");
		});

		it("should leave off the 'urn' prefix if it is already in the 'type'", function() {
			assert.equal(urn.create("urn:type", "name"), "urn:type:name");
		});
	});

	describe("parse", function() {
		it("should properly parse a proper urn with two subdivisions", function() {
			assert.deepEqual(urn.parse("urn:type:name"), {
				type: "type",
				name: "name"
			});
		});

		it("should properly parse a proper urn with more than two subdivisions", function() {
			assert.deepEqual(urn.parse("urn:typeA:typeB:name"), {
				type: "typeA:typeB",
				name: "name"
			});
		});

		it("should throw an exception if the urn is invalid", function() {
			assert.throws(()=>urn.parse("bad-u-r-n"));
		});
	});
});
