/**
 * User: curt
 * Date: 05/27/18
 * Time: 7:15 PM
 */


const proxy=require("../../lib/proxy");
const assert=require("../../lib/assert");
const codec=require("../../lib/date");

describe("codec.date", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("fromString", function() {
		it("should return null with null string", function() {
			assert.ok(codec.fromString(null)===null);
		});

		it("should return parsed date for known encoding", function() {
			const date=new Date(),
				decoded=codec.fromString(date.toISOString());
			assert.deepEqual(date, decoded);
		});

		it("should return null with invalid string", function() {
			assert.ok(codec.fromString("donkey")===null);
			assert.strictEqual(proxy.log.warn.callCount, 1);
		});
	});

	describe("fromObject", function() {
		it("should detect date type and return object itself", function() {
			const date=new Date();
			assert.strictEqual(codec.fromObject(date), date);
		});

		it("should detect string and return converted object", function() {
			const date=new Date();
			assert.deepEqual(codec.fromObject(date.toISOString()), date);
		});

		it("should detect string and return converted object", function() {
			assert.deepEqual(codec.fromObject(0), new Date(0));
		});

		it("should detect string and return converted object", function() {
			assert.strictEqual(codec.fromObject(null), null);
		});

		it("should warn and return null if it can't make sense of the input", function() {
			assert.strictEqual(codec.fromObject({}), null);
			assert.strictEqual(proxy.log.warn.callCount, 1);
		});
	});


	describe("toISOString", function() {
		it("toISOString should strip millis with no milli param", function() {
			const date=new Date(),
				encoded=codec.toISOString(date);
			assert.ok(encoded.search(/\.\d+Z$/)<0);
		});

		it("toISOString should strip millis with false param", function() {
			const date=new Date(),
				encoded=codec.toISOString(date, false);
			assert.ok(encoded.search(/\.\d+Z$/)<0);
		});

		it("toISOString should leave millis with true milli param", function() {
			const date=new Date(),
				encoded=codec.toISOString(date, true);
			assert.ok(encoded.search(/\.\d+Z$/)> -1);
		});
	});
});
