/**
 * User: curtis
 * Date: 6/3/18
 * Time: 11:39 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/template
 */

const _=require("lodash");
const {PigError}=require("./error");

const storage={
	cache: {}
};

/**
 * Does substitution of variables in template string. Encoding of variables should be as follows: {{variable}}
 * @param {string} template
 * @param {Object} variables - set of key/values
 * @param {RegExp} interpolate - regex pattern for finding substitution variables
 * @returns {string}
 */
exports.render=function(template, variables, {
	interpolate=/{{\s*(\S+?)\s*}}/g
}={}) {
	if(!storage.cache.hasOwnProperty(template)) {
		storage.cache[template]=_.template(template, {interpolate});
	}
	try {
		return storage.cache[template](variables);
	} catch(error) {
		throw new PigError({
			error,
			details: `function=${storage.cache[template].toString()}\nvariables=${JSON.stringify(variables)}`,
			message: `attempt to render template='${template}' failed`
		});
	}
};
