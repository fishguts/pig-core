/**
 * User: curtis
 * Date: 10/31/18
 * Time: 1:48 AM
 * Copyright @2018 by Xraymen Inc.
 *
 * Library of locally defined types
 */

/********************* Simple Types *********************/
/**
 * The mode that our application is running in
 * @typedef {("cli"|"gui"|"server")} PigApplicationMode
 */


/**
 * @typedef {("debug"|"verbose"|"info"|"warn"|"error"|"crit")} PigLogLevel
 */

/**
 * @typedef {("low"|"medium"|"high")} PigPriority
 */

 /**
 * @typedef {("debug"|"info"|"warn"|"error"|"fatal")} PigSeverity
 */


/********************* Complex Types *********************/
/**
 * @typedef {Object} PigLogConfiguration
 * @property {PigLogLevel} level
 * @property {Array<PigLogTransport>} transports
 */

/**
 * @typedef {Object} PigLogTransport
 * @property {("console"|"file"|"http")} type
 * @property {Object|undefined} overrides
 */
