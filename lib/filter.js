/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:52 AM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/filter
 */

const _=require("lodash");
const assert=require("./assert");

/**
 * Base class for all filters
 */
class Filter {
	/**
	 * @param {[Function]} filters
	 */
	constructor(filters) {
		this._filters=filters;
	}

	/**
	 * Return results of post filter
	 * @param {Object|Array} data
	 * @returns {Object|Array|null}
	 */
	apply(data) {
		if(!_.isArray(data)) {
			return this._isPass(data)
				? data
				: null;
		} else {
			const self=this;
			return data.reduce(function(result, object) {
				if(self._isPass(object)) {
					result.push(object);
				}
				return result;
			}, []);
		}
	}

	_isPass(object) {
		for(let index=this._filters.length-1; index>-1; index--) {
			if(!this._filters[index](object)) {
				return false;
			}
		}
		return true;
	}
}

/**
 * @typedef {Filter} StringFilter
 */
class StringFilter extends Filter {
	/**
	 * @param {Object} options
	 * 	- include {Array}
	 * 	- ignore {Array}
	 */
	constructor(options={}) {
		const filters=[];
		if(options.include) {
			assert.ok(_.isArray(options.include));
			options.include.forEach(function(text) {
				const regex=new RegExp(text);
				filters.push((text)=>text.match(regex));
			});
		}
		if(options.ignore) {
			assert.ok(_.isArray(options.ignore));
			_.get(options, "ignore", []).forEach(function(text) {
				const regex=new RegExp(text);
				filters.push((text)=>!text.match(regex));
			});
		}
		super(filters);
	}
}

exports.StringFilter=StringFilter;
