/**
 * User: curtis
 * Date: 05/27/18
 * Time: 7:51 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/validator
 * @requires ajv to perform any file based schema retrieval. You may get away without it if you serve up schemas and not file paths to schemas
 * @requires "fs-extra" to perform any file based schema retrieval. You may get away without it if you serve up schemas and not file paths to schemas
 */

/* eslint-disable no-console */

const _=require("lodash");
const _module=require("./_module");
const validator=new (_module.require("ajv"))({
	coerceTypes: "boolean"
});
const assert=require("./assert");
const http=require("./http");
const {PigError}=require("./error");

const storage={
	/**
	 * @type {Object<string, Object>}
	 */
	cache: {}
};

try {
	// we validate everything including query params which are always interpreted as strings.
	// I think we can ask him to coerce everything. If not then reduce it to boolean and numbers
} catch(error) {
	// it's okay. We will assume that this module does not require validation
}

/**
 * Whether lower level validator exists in this application
 * @returns {boolean}
 */
exports.exists=function() {
	return validator!==undefined;
};

/**
 * Adds schema to the instance.  May be a library object which other schemas reference
 * @param {Object|string} schema an object or string. If a string then it is assumed to be a path.
 * 	- paths support hash separation: /path/schema#object
 * 	@returns {Object} schema
 */
exports.addSchema=function(schema) {
	if(_.isString(schema)) {
		const fs=_module.require("fs-extra", module);

		// if we've already done business with him then return him and get out of here
		const path=schema,
			object=storage.cache[path];
		if(object!==undefined) {
			return object;
		}
		const parts=schema.split("#");
		schema=fs.readJSONSync(parts[0]);
		if(parts.length>1) {
			schema=_.get(schema, parts[1]);
			assert.ok(schema, `schema missing or data path missing: ${schema}`);
		}
		storage.cache[path]=schema;
	}
	validator.addSchema(schema);
	return schema;
};

/**
 * Validate data using the schema which should either be an object or a path
 * Schema will be compiled and cached (using serialized JSON as key, [fast-json-stable-stringify](https://github.com/epoberezkin/fast-json-stable-stringify) is used to serialize by default).
 * @param  {Object} data to be validated
 * @param  {string|Object} schema object or schema path
 * @param {Object} options and properties to mix-in with errors
 * 	- statusCode {number=400} on validation failure
 * 	- throw {Boolean=true}
 * @return {exports.PigError|null} validation result
 * @throws {PigError}
 */
exports.validateData=function(data, schema, options=undefined) {
	if(_.isString(schema)) {
		schema=exports.addSchema(schema);
	}
	return validator.validate(schema, data)
		? null
		: exports._buildErrorResponse(schema, options);
};

/**
 * Validate that schema is valid. Handy for debugging and unit testing
 * @param {Object|string} schema
 * @param {Object} options and properties to mix-in with errors
 * 	- throw {Boolean=true}
 * @returns {Error|null}
 * @throws {Error}
 */
exports.validateSchema=function(schema, options=undefined) {
	schema=exports.addSchema(schema);
	if(!validator.validateSchema(schema)) {
		return exports._buildErrorResponse(schema, options);
	}
	try {
		// this guy will force it to compile (which validateSchema does not seem to do)
		validator.getSchema(schema.$id);
	} catch(error) {
		error=new PigError({
			message: `failed to compile schema ${schema.$id}`,
			schema: schema
		});
		if(_.get(options, "throw", true)) {
			throw error;
		}
		return error;
	}
	return null;
};


/**
 * Validation has failed and this guy will gather the failure nuts
 * @param {Object} schema
 * @param {Object} options
 * @returns {PigError}
 * @throws {PigError}
 * @private
 */
exports._buildErrorResponse=function(schema, options) {
	const error=new PigError(Object.assign({
		errors: validator.errors,
		message: "schema validation failed: " + _.map(validator.errors, function(error) {
			let text="";
			if(!_.isEmpty(error.dataPath)) {
				text+=`'${_.trim(error.dataPath, ".")}' `;
			}
			if(_.has(error.params, "additionalProperty")) {
				text+=` does not support property: ${_.trim(error.params.additionalProperty, ".")}`;
			} else if(_.has(error.params, "missingProperty")) {
				text+=`should have required property: ${_.trim(error.params.missingProperty, ".")}`;
			} else {
				text+=error.message;
			}
			return text;
		}).join("\n"),
		schema: schema,
		statusCode: http.status.code.BAD_REQUEST
	}, _.omit(options, "throw")));
	if(_.get(options, "throw", true)) {
		throw error;
	}
	return error;
};
