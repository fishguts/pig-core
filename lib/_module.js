/**
 * User: curtis
 * Date: 10/31/18
 * Time: 7:47 PM
 * Copyright @2018 by Xraymen Inc.
 */

const {PigError}=require("./error");

/* eslint-disable no-console */
/**
 * Attempts to load a package and reports those that are missing and throws an exception meant to not be handled
 * @param name
 * @param module
 * @returns {*}
 * @throws PigError
 */
exports.require=function(name, module) {
	try {
		return require(name);
	} catch (error) {
		console.error(`pig-core.proxy: ${name} is not included in the current environment`);
		throw new PigError({
			details: `must include ${name} package to use ${module.filename}`,
			message: "Missing Dependencies"
		});
	}
};

/**
 * Are we in a node environment?
 * @returns {Boolean}
 */
exports.testForNodeJS=function(throwOnNone=false) {
	try {
		return /^\d+\.\d+\.\d+$/.test(process.versions.node);
	} catch(error) {
		return false;
	}
};
