/**
 * User: curtis
 * Date: 10/31/18
 * Time: 1:41 AM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/log
 */

const _module=require("../_module");

try {
	if(_module.testForNodeJS()===false) {
		throw new Error("no NodeJS");
	}
	// let's see if we have access to winston and act accordingly
	require.resolve("winston");
	module.exports=require("./winston");
} catch(error) {
	module.exports=require("./console");
}
