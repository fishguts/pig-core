/**
 * User: curtis
 * Date: 05/27/18
 * Time: 10:02 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/log/winston
 * @requires node
 * @requires "fs-extra"
 * @requires winston
 */

const _=require("lodash");
const _module=require("../_module");
const fs=_module.require("fs-extra", module);
const path=require("path");
const util=require("util");
const winston=_module.require("winston", module);
const assert=require("../assert");
const format=require("../format");

const storage={
	/**
	 * @type {Logger}
	 */
	logger: null,
	/**
	 * Optional. If accessed then it will be built
	 * @type {Object}
	 */
	middle: null
};


/**** Public Interface ****/
exports.level={
	/**
	 * Get log level threshold
	 * @return {PigLogLevel}
	 */
	get: ()=>_.getLogger().level,
	/**
	 * Set log level threshold
	 * @param {PigLogLevel} level
	 * @return {PigLogLevel}
	 */
	set: (level)=>_.getLogger().level=level,
	/**
	 * todo: we want to replace this with proper filtering
	 * @param {PigLogLevel} level
	 * @return {boolean}
	 */
	isLog: (level)=>true
};

/**
 * Configures the logger. You should perform configuration early in the startup of your application.
 * Note: The default configuration routes output to the console with localized metadata.
 * @param {string} applicationName
 * @param {PigLogConfiguration} configuration
 * @param {string} environment
 */
exports.configure=function({applicationName, configuration, environment}) {
	storage.logger=_buildLogger({applicationName, configuration, environment});
};

/**
 * Only for internal 'cause We write this guy directly to stderr to avoid caching.
 * Note: the reason we write to error by default is 'cause we assume that it's out of the ordinary logging.
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 * @param {WriteStream} stream
 * @return {Promise}
 */
exports.console=function(text, {
	meta=undefined,
	stack=false,
	stream=process.stderr
}={}) {
	text=format.messageToString(text, {stack});
	if(meta) {
		text=`${text}\n${JSON.stringify(text, null, "\t")}`;
	}
	return util.promisify(stream.write.bind(process.stdout))(text, "utf8");
};

/**
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.debug=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("debug")) {
		text=format.messageToString(text, {stack});
		_getLogger().debug(text, meta);
	}
};

/**
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.diag=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("debug")) {
		text=format.messageToString(text, {stack});
		_getLogger().debug(text, Object.assign({diagnostic: true}, meta));
	}
};

/**
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.verbose=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("verbose")) {
		text=format.messageToString(text, {stack});
		_getLogger().verbose(text, meta);
	}
};

/**
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.info=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("info")) {
		text=format.messageToString(text, {stack});
		_getLogger().info(text, meta);
	}
};

/**
 * Logs text or error. If <code>text</code> is an error by the time it gets here then we will apply
 * <code>errorToString</code> to it before writing it to the log (and will add it as metadata?)
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.warn=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("warn")) {
		text=format.messageToString(text, {stack});
		_getLogger().warn(text, meta);
	}
};

/**
 * Logs text or error. If <code>text</code> is an error by the time it gets here then we will apply
 * <code>errorToString</code> to it before writing it to the log (and will add it as metadata?)
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.error=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("error")) {
		text=format.messageToString(text, {stack});
		_getLogger().error(text, meta);
	}
};

/**
 * Returns a function that matches the logging policy
 * @param {string} policy
 * @returns {Function}
 */
exports.policyToFunction=(policy)=>_.get(exports, policy, exports.debug);

/**
 * Gets middleware for insertion into express or wherever you like I imagine
 * @returns {Function}
 */
exports.middleware=_getMiddleware;


/**** Private Interface and action****/
/**
 * Makes sure our logger is created and then returns it
 * @returns {Logger}
 * @private
 */
function _getLogger() {
	if(!storage.logger) {
		// Note: we used to log a warning here about it not being configured. But that causes misleading logging during
		// startup that is hard to get around and causes grief with integration tests and startup errors. It's more
		// trouble than it's worth. Hey, configure it on startup! In the meantime we will stash a default configuration.
		storage.logger=_buildLogger({
			applicationName: "pig-core",
			configuration: {
				level: "debug",
				transports: [{
					type: "console"
				}]
			},
			environment: "unknown"
		});
	}
	return storage.logger;
}

/**
 * Makes sure middleware is created and then returns it
 * @returns {Function}
 * @private
 */
function _getMiddleware() {
	if(!storage.middle) {
		storage.middle=_buildMiddleware();
	}
	return storage.middle;
}

/**
 * Builds/rebuilds the logger as per current application configuration
 * @param {string} applicationName
 * @param {PigLogConfiguration} configuration
 * @param {string} environment
 * @returns {Logger}
 * @private
 */
function _buildLogger({applicationName, configuration, environment}) {
	function _conditionData(info) {
		// there are things about our options that direct our behavior but that we don't need in the logs
		if(info.options) {
			delete info.options.logLevel;
			delete info.options.verbose;
		}
		return info;
	}

	function _conditionHttpData(info) {
		info=_conditionData(info);
		info.service=applicationName;
		info.environment=environment;
		return info;
	}

	const transports=_.map(configuration.transports, function(transport) {
		switch(transport.type) {
			case "console": {
				const _formatMessage=(info)=>{
					info=_conditionData(info);
					// We don't want es6 symbols nor properties we report directly in our console metadata.
					const pruned=_.pick(info,
						_(info)
							.keys()
							.without("level", "message", "timestamp")
							.value());
					const metadata=(transport.lean || _.isEmpty(pruned))
						? null
						: util.inspect(pruned, {
							breakLength: Infinity,
							colors: true,
							depth: null
						});
					return (metadata)
						? `${info.timestamp} ${info.level.toUpperCase()}: ${info.message}, metadata=${metadata}`
						: `${info.timestamp} ${info.level.toUpperCase()}: ${info.message}`;
				};

				return new winston.transports.Console(Object.assign({
					format: winston.format.combine(
						winston.format.timestamp(),
						winston.format.printf(_formatMessage)
					)
				}, transport.overrides));
			}
			case "file": {
				fs.ensureDirSync("log");
				return new winston.transports.File(Object.assign({
					filename: path.join("log", _.get(transport.overrides, "filename", applicationName)),
					format: winston.format.combine(
						winston.format.timestamp(),
						winston.format(_conditionData)(),
						winston.format.json()
					)
				}, _.omit(transport.overrides, "filename")));
			}
			case "http": {
				assert.ok(transport.overrides.host);
				return new winston.transports.Http(Object.assign({
					format: winston.format.combine(
						winston.format.timestamp(),
						winston.format(_conditionHttpData)(),
						winston.format.json()
					)
				}, transport.overrides));
			}
			default: {
				throw new Error(`unknown transport type '${transport.type}'`);
			}
		}
	});
	return winston.createLogger({
		level: configuration.level,
		transports: transports
	});
}

/**
 * We consider this optional.  But if somebody tries to build it then it better be included in our package
 * @param {Object} application
 * @private
 */
function _buildMiddleware(application) {
	/* eslint-disable no-unused-vars */
	const morgan=_module.require("morgan", module);
	// todo: build him
}
