/**
 * User: curtis
 * Date: 3/5/2018
 * Time: 9:10 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * @module pig-core/log/console
 */

const constant=require("../constant");
const format=require("../format");

const storage={
	logger: null
};


/* eslint-disable no-console */

/**** Public Interface ****/
exports.level={
	/**
	 * Get log level threshold
	 * @return {PigLogLevel}
	 */
	get: ()=>_getLogger().level,
	/**
	 * Set log level threshold
	 * @param {PigLogLevel} level
	 * @return {PigLogLevel}
	 */
	set: (level)=>_getLogger().level=level,
	/**
	 * todo: we want to replace this with proper filtering
	 * @param {PigLogLevel} level
	 * @return {boolean}
	 */
	isLog: (level)=>constant.severity.trips(level, _getLogger().level)
};

/**
 * Configures the logger. You should perform configuration early in the startup of your application.
 * Note: The default configuration routes output to the console with localized metadata.
 * @param {string} applicationName
 * @param {PigLogConfiguration} configuration
 * @param {string} environment
 */
exports.configure=function({applicationName, configuration, environment}) {
	storage.logger=_buildLogger({applicationName, configuration, environment});
};

/**
 * For compatibility with our <code>log</code> interface
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 * @param {Function} stream
 * @return {Promise}
 */
exports.console=function(text, {
	meta=undefined,
	stack=false,
	stream=console.error
}={}) {
	text=format.messageToString(text, {stack});
	if(meta) {
		text=`${text}\n${JSON.stringify(text, null, "\t")}`;
	}
	stream(text);
	return Promise.resolve();
};

/**
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.debug=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("debug")) {
		text=format.messageToString(text, {stack});
		if(meta!==undefined) {
			_getLogger().debug(text, meta);
		} else {
			_getLogger().debug(text);
		}
	}
};

/**
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.diag=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("debug")) {
		text=format.messageToString(text, {stack});
		_getLogger().debug(text, Object.assign({diagnostic: true}, meta));
	}
};

/**
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.verbose=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("verbose")) {
		text=format.messageToString(text, {stack});
		if(meta!==undefined) {
			_getLogger().verbose(text, meta);
		} else {
			_getLogger().verbose(text);
		}
	}
};

/**
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.info=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("info")) {
		text=format.messageToString(text, {stack});
		if(meta!==undefined) {
			_getLogger().info(text, meta);
		} else {
			_getLogger().info(text);
		}
	}
};

/**
 * Logs text or error. If <code>text</code> is an error by the time it gets here then we will apply
 * <code>errorToString</code> to it before writing it to the log (and will add it as metadata?)
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.warn=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("warn")) {
		text=format.messageToString(text, {stack});
		if(meta!==undefined) {
			_getLogger().warn(text, meta);
		} else {
			_getLogger().warn(text);
		}
	}
};

/**
 * Logs text or error. If <code>text</code> is an error by the time it gets here then we will apply
 * <code>errorToString</code> to it before writing it to the log (and will add it as metadata?)
 * @param {Error|string|Function} text
 * @param {Object} meta
 * @param {boolean} stack
 */
exports.error=function(text, {
	meta=undefined,
	stack=false
}={}) {
	if(exports.level.isLog("error")) {
		text=format.messageToString(text, {stack});
		if(meta!==undefined) {
			_getLogger().error(text, meta);
		} else {
			_getLogger().error(text);
		}
	}
};

/**** Private Interface and action****/
/**
 * Makes sure logger is created and then returns it
 * @returns {{verbose: Function, debug: Function, info: Function, warn: Function, error: Function}}
 * @private
 */
function _getLogger() {
	if(!storage.logger) {
		// Note: we used to log a warning here about it not being configured. But that causes misleading logging during
		// startup that is hard to get around and causes grief with integration tests and startup errors. It's more
		// trouble than it's worth. Hey, configure it on startup! In the meantime we will stash a default configuration.
		storage.logger=_buildLogger({
			applicationName: "pig-core",
			configuration: {
				level: "debug",
				transports: [{
					type: "console"
				}]
			},
			environment: "unknown"
		});
	}
	return storage.logger;
}

/**
 * Builds/rebuilds the logger as per current application configuration
 * @param {string} applicationName
 * @param {PigLogConfiguration} configuration
 * @param {string} environment
 * @returns {{verbose: Function, debug: Function, info: Function, warn: Function, error: Function}}
 * @private
 */
function _buildLogger({applicationName, configuration, environment}) {
	return {
		verbose: console.debug,
		debug: console.debug,
		info: console.info,
		warn: console.warn,
		error: console.error
	};
}
