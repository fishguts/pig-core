/**
 * User: curt
 * Date: 3/5/2018
 * Time: 9:10 PM
 *
 * @module pig-core/constant
 */

/**
 * Determines whether the value is valid
 * @param {Object} enumObject
 * @param {string} value
 * @returns {boolean}
 */
exports.isValidValue=function(enumObject, value) {
	return Object.values(enumObject)
		.indexOf(value)> -1;
};

/**
 * Constants having to do with <code>this</code> running application.
 * Should be in parity with <code>PigApplicationMode</code>
 */
exports.application={
	mode: {
		CLI: "cli",
		GUI: "gui",
		SERVER: "server"
	}
};
/**
 * @type {function(value:string):Boolean}
 */
exports.isValidApplicationMode=exports.isValidValue.bind(null, exports.application.mode);

/**
 * Notification events
 */
exports.event={
	SETTINGS_LOADED: "settings.loaded",
	SHUTDOWN_START: "shutdown.start",
	STARTUP_COMPLETE: "startup.complete",
	STARTUP_FAILED: "startup.failed"
};
/**
 * @type {function(value:string):Boolean}
 */
exports.isValidEvent=exports.isValidValue.bind(null, exports.event);

/**
 * Priority values that we know about
 * Should by in parity with <code>PigPriority</code>
 */
exports.priority={
	LOW: "low",
	MEDIUM: "medium",
	HIGH: "high"
};
/**
 * @type {function(value:string):Boolean}
 */
exports.isValidPriority=exports.isValidValue.bind(null, exports.priority);

/**
 * Error severities.
 * Should by in parity with <code>PigSeverity</code>
 */
exports.severity={
	DEBUG: "debug",
	INFO: "info",
	WARN: "warn",
	ERROR: "error",
	FATAL: "fatal",

	/**
	 * Is the specified <param>severity</param> greater than or equal to the threshold value which is "warn" by default
	 * @param {PigSeverity} value
	 * @param {PigSeverity} threshold
	 * @returns {boolean}
	 */
	trips: (value, threshold=exports.severity.WARN)=>{
		return exports.severity._ORDER.indexOf(value)>=exports.severity._ORDER.indexOf(threshold);
	},

	/**
	 * The order of severity for measurement
	 * @private
	 */
	_ORDER: ["debug", "info", "warn", "error", "fatal"]
};

/**
 * @type {function(value:string):Boolean}
 */
exports.isValidSeverity=exports.isValidValue.bind(null, exports.severity);
