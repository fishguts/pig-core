/**
 * User: curtis
 * Date: 05/27/18
 * Time: 10:10 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/notification
 */

let emitter;

try {
	const {EventEmitter}=require("events");
	emitter=new EventEmitter();
} catch(error) {
	throw new Error("not implemented properly");
}

/**
 * @param {string} event
 * @param {function(name:String, param:Object):void} handler
 */
exports.addListener=function(event, handler) {
	emitter.addListener(event, handler);
};

/**
 * @param {string} event
 * @param {function(name:String, param:Object):void} handler
 */
exports.removeListener=function(event, handler) {
	emitter.removeListener(event, handler);
};

/**
 * @param {string} event
 * @param {*} param
 */
exports.emit=function(event, param=undefined) {
	emitter.emit(event, param);
};

