/**
 * User: curtis
 * Date: 2/14/18
 * Time: 1:47 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/file
 * @requires node
 * @requires "fs-extra"
 */

const _module=require("./_module");
const path=_module.require("path", module);
const fs=_module.require("fs-extra", module);
const jsyaml=require("js-yaml");
const util=require("./util");

/**
 * Converts a path relative to the specified <param>module</param> into an absolute path.
 * What is the use of this guy? Nothing really if you are an application. But if you are a library
 * then he sorts out the issues having to do with <code>cwd</code> vs. the package root.
 * Note: you must specify <param>relative</param> or <param>relativeToModuleRoot</param> but not both
 * @param {{filename: string, paths: Array<string>}} module
 * @param {string} relative
 * @param {string} relativeToProjectRoot - allows us to use paths that are relative from a modules root.
 * @returns {string}
 */
exports.relativePathToAbsolute=function({module, relative, relativeToModuleRoot}) {
	if(relativeToModuleRoot) {
		// little bit hacky but effective at finding the project's root
		for(let index=0; index<module.paths.length; index++) {
			if(fs.pathExistsSync(module.paths[index])) {
				let root=module.paths[index].substr(0, module.paths[index].length-"node_modules".length);
				return path.join(root, relativeToModuleRoot);
			}
		}
	}
	const parsed=path.parse(module.filename);
	return path.resolve(parsed.dir, relative);
};

/**
 * Determines from path how file is encoded and attempts to do the right thing
 * @param {string} uri
 * @param {string} encoding
 * @param {Object} options
 * @param {Function} callback
 * @return {Object}
 */
exports.readToJSON=function(uri, {
	encoding="utf-8",
	...options
}, callback) {
	const parsed=path.parse(uri);
	options=Object.assign({encoding, ...options});
	fs.readFile(uri, options, (error, data)=>{
		if(!error) {
			if(parsed.ext.toLowerCase()===".yaml") {
				data=jsyaml.safeLoad(data);
			} else {
				data=JSON.parse(data);
			}
		}
		callback(error, data);
	})
};

/**
 * Determines from path how file is encoded and attempts to do the right thing
 * @param {string} uri
 * @param {string} encoding
 * @param {Object} options
 * @return {Object}
 */
exports.readToJSONSync=function(uri, {
	encoding="utf-8",
	...options
}={}) {
	const parsed=path.parse(uri);
	options=Object.assign({encoding, ...options});
	if(parsed.ext.toLowerCase()===".yaml") {
		return jsyaml.safeLoad(fs.readFileSync(uri, options));
	} else {
		return fs.readJSONSync(uri, options);
	}
};

/**
 * @param {string} uri
 * @param {*} data
 * @param {Boolean} async
 * @param {Boolean} createPath
 * @param {string} encoding
 * @param {string} flag
 * @param {number} mode
 * @returns {Promise<boolean>|undefined}
 * @throws {Error} only if in sync mode
 */
exports.writeJSON=function(uri, data, {
	async=true,
	createPath=true,
	encoding="utf8",
	flag=undefined,
	mode=undefined
}) {
	if(typeof(data)!=="string") {
		data=JSON.stringify(data);
	}
	return exports.writeFile(uri, data, {async, createPath, encoding, flag, mode});
};


/**
 * @param {string} uri
 * @param {*} data
 * @param {Boolean} async
 * @param {Boolean} createPath
 * @param {string} encoding
 * @param {string} flag
 * @param {number} mode
 * @returns {Promise<boolean>|undefined}
 * @throws {Error} only if in sync mode
 */
exports.writeFile=function(uri, data, {
	async=true,
	createPath=true,
	encoding=undefined,
	flag=undefined,
	mode=undefined
}) {
	const parsed=path.parse(uri),
		options=util.scrubObject({encoding, flag, mode});
	if(async) {
		return fs.pathExists(parsed.dir)
			.then((exists)=>{
				return (!exists && createPath)
					? fs.mkdirs(parsed.dir)
					: Promise.resolve();
			})
			.then(()=>fs.writeFile(uri, data, options));
	} else {
		if(!fs.pathExistsSync(parsed.dir)) {
			if(createPath) {
				fs.mkdirsSync(parsed.dir);
			}
		}
		fs.writeFileSync(uri, data, options);
	}
};

