/**
 * User: curtis
 * Date: 12/2/17
 * Time: 8:23 PM
 * Copyright @2018 by Xraymen Inc.
 * 
 * @module pig-core/json
 */

const {PigError}=require("./error");

/**
 * Simple little guy that we end up doing over and over
 * @param {String} text
 * @param {Function} callback used if specified otherwise throws errors
 * @returns {Object}
 */
exports.parse=function(text, callback=null) {
	try {
		const parsed=JSON.parse(text);
		return (callback)
			? callback(null, parsed)
			: parsed;
	} catch(error) {
		error=new PigError({
			message: `unable to parse JSON: ${error}`,
			error: error,
			text: text
		});
		if(callback) {
			callback(error);
		} else {
			throw error;
		}
	}
};
