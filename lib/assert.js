/**
 * User: curt
 * Date: 3/5/2018
 * Time: 9:10 PM
 *
 * @module pig-core/assert
 *
 *  There is both runtime support and test support. We flag those that are safe for any runtime environment.
 * But most are intended for unit and integration testing
 */

const _=require("lodash");
const _module=require("./_module");
const configuration=require("./configuration");
const {PigError}=require("./error");
const diagnostics=require("./diagnostics");
const format=require("./format");
const util=require("./util");

let _deepEqual;

if(_module.testForNodeJS()) {
	const assert=require("assert");
	// mixin the guys that we like
	_deepEqual=assert.deepEqual;
	exports.deepStrictEqual=assert.deepStrictEqual;
	exports.doesNotThrow=assert.doesNotThrow;
	exports.equal=assert.equal;
	exports.ifError=assert.ifError;
	exports.notDeepEqual=assert.notDeepEqual;
	exports.notEqual=assert.notEqual;
	exports.notStrictEqual=assert.notStrictEqual;
	exports.ok=assert.ok;
	exports.strictEqual=assert.strictEqual;
	exports.throws=assert.throws;
} else {
	// eslint-disable-next-line no-console
	const _notImplemented=(name)=>console.warn(`assert.${name} not supported in current environment`);

	exports.deepStrictEqual=_notImplemented.bind(null, "deepStrictEqual");
	exports.doesNotThrow=_notImplemented.bind(null, "doesNotThrow");
	exports.equal=_notImplemented.bind(null, "equal");
	exports.ifError=_notImplemented.bind(null, "ifError");
	exports.notDeepEqual=_notImplemented.bind(null, "notDeepEqual");
	exports.notEqual=_notImplemented.bind(null, "notEqual");
	exports.notStrictEqual=_notImplemented.bind(null, "notStrictEqual");
	exports.strictEqual=_notImplemented.bind(null, "strictEqual");
	exports.throws=_notImplemented.bind(null, "throws");

	_deepEqual=(actual, expected, message)=>{
		if(!_.isEqual(actual, expected)) {
			throw new PigError({
				actual, expected,
				details: format.messageToString(message),
				message: "assert.deepEqual() failed"
			});
		}
	};

	exports.ok=function(condition, message=undefined) {
		if(!condition) {
			throw new PigError({
				details: format.messageToString(message),
				message: `assert.ok(${condition}==true) failed`
			});
		}
	};
}

/**
 * We print out the expected as, in here at least, we frequently want to steal it.
 * @param {*} actual
 * @param {*} expected
 * @param {string} message
 * @param {boolean} scrub
 * @throws {Error}
 */
exports.deepEqual=function(actual, expected, {
	message=undefined,
	scrub=true
}={}) {
	try {
		if(scrub) {
			actual=util.scrubObject(actual);
			expected=util.scrubObject(expected);
		}
		actual=util.objectToData(actual, {sort: true});
		expected=util.objectToData(expected, {sort: true});
		_deepEqual(actual, expected, message);
	} catch(error) {
		// log.console returns a Promise. We would like to respond asynchronously so that we could be sure that
		// the buffer is entirely written. But cannot assume async behavior at this level. Let's see how it goes.
		configuration.log.console(`assert.deepEqual() failed: actual=\n${JSON.stringify(actual, null, "\t")}`);
		throw error;
	}
};

/**
 * macro for assert.ok(false, error)
 * @param {Error|string} error
 */
exports.fail=(error)=>{
	// note: we convert it to a string (if an error) so that the assert library doesn't just throw him
	exports.ok(false, error.toString());
};

/**
 * Asserts there is an error. Supports async business and returns a callable function should <param>error</param> be a function.
 * @param {Error|Function|null} error
 * @returns {Function|undefined}
 * @throws {Error}
 * @example for async use
 *   seriesQueue.execute(assert.notError(done));
 */
exports.isError=function(error) {
	if(_.isFunction(error)) {
		// we capture the stack now so that we can tie errors back to the origin
		const stack=diagnostics.getStack({popCount: 2});
		return function(_error) {
			if(!_error) {
				exports.ok(false, `assert.isError(): ${_error.message}\n${stack}`);
			} else {
				error();
			}
		};
	} else if(!error) {
		exports.ok(false, "Expecting error");
	}
};

/**
 * Asserts if there is an error. Supports async business and returns a callable function should <param>error</param> be a function.
 * @param {Error|Function|null} error
 * @returns {Function|undefined}
 * @throws {Error}
 * @example for async use
 *   seriesQueue.execute(assert.isNotError(done));
 */
exports.isNotError=function(error) {
	if(_.isFunction(error)) {
		// we capture the stack now so that we can tie errors back to the origin
		const stack=diagnostics.getStack({popCount: 2});
		return function(_error) {
			if(!_error) {
				error();
			} else if(_error.constructor.name==="AssertionError") {
				// this is already an assertion. Let's not double up. It clouds the stack waters
				error(_error);
			} else {
				exports.ok(false, `assert.isNotError(): ${_error.message}\n${stack}`);
			}
		};
	} else if(error) {
		exports.ok(false, error);
	}
};
/**
 * Simply asserts false if called
 * @param {string|Error|Function} [message]
 * @throws {Error}
 */
exports.notCalled=function(message="should not have been called") {
	throw new PigError({
		details: format.messageToString(message),
		message: "assert.notCalled() failed"
	});
};


/**
 * Will assert (to log) that all objects passed as params remain unchanged.
 * @param {[Object]} objects
 * @returns {function():@throws} - call when you are done and want to make sure the object did not change
 * @throws {Error}
 */
exports.immutable=function(...objects) {
	const clones=_.map(objects, (object)=>[_.cloneDeep(object), object]);
	return function() {
		clones.forEach(function(pair) {
			if(!_.isEqual(pair[0], pair[1])) {
				const expected=JSON.stringify(pair[0], null, "\t"),
					actual=JSON.stringify(pair[1], null, "\t");
				throw new PigError({
					details: `expected=${expected}\nactual=${actual}`,
					message: "assert.immutable() failed"
				});
			}
		});
	};
};


/**
 * Asserts that all properties exist on object
 * @param {Object} object
 * @param {string|Array} property
 * @throws {Error}
 */
exports.properties=function(object, property) {
	function _assert(_property) {
		if(!_.has(object, _property)) {
			throw new PigError({
				details: `${property} does not exist in ${JSON.stringify(object)}`,
				message: `assert.properties(${property}) failed`
			});
		}
	}
	if(_.isString(property)) {
		_assert(property);
	} else {
		property.forEach(_assert);
	}
};

/**
 * Asserts that an object is valid for the specified schema. ONLY executes in <param>debug</param> and
 * if there is a validation module in the current application.
 * @param {Object} object
 * @param {Object|String} schema - either a schema object or a path
 * @param {Boolean} debug - means of detecting whether lib is in debug mode or not
 * @throws {Error}
 */
exports.schema=function(object, schema, {
	debug=process.env.DEBUG
}={}) {
	if(debug) {
		const validator=require("./validator");
		try {
			validator.validateData(object, schema);
		} catch(error) {
			exports.fail(error);
		}
	}
};
/**
 * Assert condition and if it is false then logs it
 * @param {boolean} condition
 * @param {string|Error|Function} [message]
 * @throws {Error}
 */
exports.toLog=function(condition, message="") {
	if(!condition) {
		configuration.log.error(new PigError({
			details: format.messageToString(message),
			message: "assert.toLog() failed"
		}));
	}
};
