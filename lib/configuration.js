/**
 * User: curtis
 * Date: 11/14/18
 * Time: 11:56 AM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/configuration
 *
 * Configure this lib to your tastes
 */

const storage={
	defaults: {
		log: undefined
	}
};

module.exports={
	/**
	 * This is the log this library uses for logging. By default it is the log imported by <code>./index.js</code>. Most likely this will be
	 * fine. But considering multiple instances of this library could exist in a single application (via modules) it is probably always a good
	 * idea to be explicit.
	 * @returns {module:pig-core/log}
	 */
	get log() {
		return storage.defaults.log || (storage.defaults.log=require("./index").log);
	},
	set log(value) {
		storage.defaults.log=value;
	}
};
