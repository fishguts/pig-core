/**
 * User: curtis
 * Date: 3/12/18
 * Time: 9:50 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core/http
 */


const _=require("lodash");
const assert=require("./assert");

/**
 * Some popular "Content-Type"s
 */
const content={
	type: {
		HTML: "text/html",
		GIF: "image/gif",
		JSON: "application/json",
		JPG: "image/jpeg",
		MPEG: "video/mpeg",
		PNG: "image/png",
		multi: {
			FORM: "multipart/form-data",
			RELATED: "multipart/related"
		}
	}
};

/**
 * Some popular HTTP methods
 */
const method={
	CONNECT: "CONNECT",
	DELETE: "DELETE",
	GET: "GET",
	HEAD: "HEAD",
	OPTIONS: "OPTIONS",
	POST: "POST",
	PUT: "PUT"
};

const status={
	/**
	 * Status numbers
	 */
	code: {
		CONTINUE: 100,
		SWITCHING_PROTOCOLS: 101,
		PROCESSING: 102,
		OK: 200,
		CREATED: 201,
		ACCEPTED: 202,
		RESET_CONTENT: 205,
		NON_AUTHORITATIVE_INFORMATION: 203,
		NO_CONTENT: 204,
		PARTIAL_CONTENT: 206,
		MULTI_STATUS: 207,
		MULTIPLE_CHOICES: 300,
		MOVED_PERMANENTLY: 301,
		MOVED_TEMPORARILY: 302,
		SEE_OTHER: 303,
		NOT_MODIFIED: 304,
		USE_PROXY: 305,
		TEMPORARY_REDIRECT: 307,
		PERMANENT_REDIRECT: 308,
		UNAUTHORIZED: 401,
		BAD_REQUEST: 400,
		PAYMENT_REQUIRED: 402,
		FORBIDDEN: 403,
		NOT_FOUND: 404,
		METHOD_NOT_ALLOWED: 405,
		NOT_ACCEPTABLE: 406,
		PROXY_AUTHENTICATION_REQUIRED: 407,
		CONFLICT: 409,
		GONE: 410,
		LENGTH_REQUIRED: 411,
		METHOD_FAILURE: 420,
		REQUEST_TIMEOUT: 408,
		REQUEST_TOO_LONG: 413,
		REQUEST_URI_TOO_LONG: 414,
		REQUESTED_RANGE_NOT_SATISFIABLE: 416,
		UNSUPPORTED_MEDIA_TYPE: 415,
		PRECONDITION_FAILED: 412,
		EXPECTATION_FAILED: 417,
		INSUFFICIENT_SPACE_ON_RESOURCE: 419,
		UNPROCESSABLE_ENTITY: 422,
		LOCKED: 423,
		FAILED_DEPENDENCY: 424,
		PRECONDITION_REQUIRED: 428,
		TOO_MANY_REQUESTS: 429,
		REQUEST_HEADER_FIELDS_TOO_LARGE: 431,
		INTERNAL_SERVER_ERROR: 500,
		NOT_IMPLEMENTED: 501,
		BAD_GATEWAY: 502,
		SERVICE_UNAVAILABLE: 503,
		GATEWAY_TIMEOUT: 504,
		HTTP_VERSION_NOT_SUPPORTED: 505,
		INSUFFICIENT_STORAGE: 507,
		NETWORK_AUTHENTICATION_REQUIRED: 511
	},
	/**
	 * Keyed by the status code.
	 */
	text: {
		100: "Continue",
		101: "Switching Protocols",
		102: "Processing",
		200: "OK",
		201: "Created",
		202: "Accepted",
		203: "Non-Authoritative Information",
		204: "No Content",
		205: "Reset Content",
		206: "Partial Content",
		207: "Multi-Status",
		208: "Already Reported",
		226: "IM Used",
		300: "Multiple Choices",
		301: "Moved Permanently",
		302: "Found",
		303: "See Other",
		304: "Not Modified",
		305: "Use Proxy",
		307: "Temporary Redirect",
		308: "Permanent Redirect",
		400: "Bad Request",
		401: "Unauthorized",
		402: "Payment Required",
		403: "Forbidden",
		404: "Not Found",
		405: "Method Not Allowed",
		406: "Not Acceptable",
		407: "Proxy Authentication Required",
		408: "Request Timeout",
		409: "Conflict",
		410: "Gone",
		411: "Length Required",
		412: "Precondition Failed",
		413: "Payload Too Large",
		414: "URI Too Long",
		415: "Unsupported Media Type",
		416: "Range Not Satisfiable",
		417: "Expectation Failed",
		418: "I'm a teapot",
		421: "Misdirected Request",
		422: "Unprocessable Entity",
		423: "Locked",
		424: "Failed Dependency",
		425: "Unordered Collection",
		426: "Upgrade Required",
		428: "Precondition Required",
		429: "Too Many Requests",
		431: "Request Header Fields Too Large",
		451: "Unavailable For Legal Reasons",
		500: "Internal Server Error",
		501: "Not Implemented",
		502: "Bad Gateway",
		503: "Service Unavailable",
		504: "Gateway Timeout",
		505: "HTTP Version Not Supported",
		506: "Variant Also Negotiates",
		507: "Insufficient Storage",
		508: "Loop Detected",
		509: "Bandwidth Limit Exceeded",
		510: "Not Extended",
		511: "Network Authentication Required"
	}
};

/**
 * Makes a string url out of the various means of tracking one
 */
class Url {
	/**
	 * @param {Object} options
	 * @param {string|Url|undefined} options.url
	 * @param {string|undefined} options.host
	 * @param {string|undefined} options.method
	 * @param {string|undefined} options.path
	 * @param {string|undefined} options.port
	 * @param {string|undefined} options.protocol
	 * @param {string} path yes, it can be specified in options but it's fairly frequent that we want
	 *    to add a path to a server host:port. This guy just makes that easy to mash the two up.
	 */
	constructor(options, path=undefined) {
		assert.ok(options.url || options.host);
		if(options.url instanceof Url) {
			this.options=Object.assign({}, options.url.options, _.omit(options, "url"));
		} else {
			// we clone it so that we can modify it
			this.options=Object.assign({}, options);
		}
		if(path) {
			this.options.path=path;
		}
		// make these guys the same
		this.toString=this.build;
	}

	/**
	 * Gets the partial or full path
	 * @param {string} suffix to append if any
	 * @returns {string}
	 */
	build(suffix="") {
		let prefix=this.options.url,
			_join=function() {
				let result=arguments[0];
				for(let i=1; i<arguments.length; i++) {
					if(arguments[i]) {
						if(!(_.endsWith(result, "/") || _.startsWith(arguments[i], "/"))) {
							result+="/";
						}
						result+=arguments[i];
					}
				}
				return result;
			};
		if(prefix) {
			if(!prefix.match(/^https?:\/\/$/)) {
				prefix=`${this.options.protocol || "http"}://${prefix}`;
			}
		} else {
			prefix=`${(this.options.port===443) ? "https" : "http"}://${this.options.host}`;
			if(this.options.port) {
				prefix+=`:${this.options.port}`;
			}
		}
		return _join(prefix, this.options.path, suffix);
	}
}

module.exports={
	content,
	method,
	status,
	Url
};
