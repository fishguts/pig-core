/**
 * User: curtis
 * Date: 10/31/18
 * Time: 2:15 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-core
 */

const storage={
	assert: null,
	constant: null,
	date: null,
	diagnostics: null,
	error: null,
	file: null,
	filter: null,
	format: null,
	http: null,
	introspect: null,
	json: null,
	log: null,
	mutation: null,
	notification: null,
	promise: null,
	proxy: null,
	template: null,
	types: null,
	urn: null,
	util: null,
	validator: null
};

module.exports={
	/**
	 * @type {module:pig-core/assert}
	 */
	get assert() {
		return storage.assert || (storage.assert=require("./assert"));
	},
	/**
	 * @type {module:pig-core/configuration}
	 */
	configuration: require("./configuration"),
	/**
	 * @type {module:pig-core/constant}
	 */
	get constant() {
		return storage.constant || (storage.constant=require("./constant"));
	},
	/**
	 * @type {module:pig-core/date}
	 */
	get date() {
		return storage.date || (storage.date=require("./date"));
	},
	/**
	 * @type {module:pig-core/diagnostics}
	 */
	get diagnostics() {
		return storage.diagnostics || (storage.diagnostics=require("./diagnostics"));
	},
	/**
	 * @type {module:pig-core/error}
	 */
	get error() {
		return storage.error || (storage.error=require("./error"));
	},
	/**
	 * @type {module:pig-core/file}
	 */
	get file() {
		return storage.file || (storage.file=require("./file"));
	},
	/**
	 * @type {module:pig-core/filter}
	 */
	get filter() {
		return storage.filter || (storage.filter=require("./filter"));
	},
	/**
	 * @type {module:pig-core/format}
	 */
	get format() {
		return storage.format || (storage.format=require("./format"));
	},
	/**
	 * @type {module:pig-core/http}
	 */
	get http() {
		return storage.http || (storage.http=require("./http"));
	},
	/**
	 * @type {module:pig-core/introspect}
	 */
	get introspect() {
		return storage.introspect || (storage.introspect=require("./introspect"));
	},
	/**
	 * @type {module:pig-core/json}
	 */
	get json() {
		return storage.json || (storage.json=require("./json"));
	},
	/**
	 * @type {module:pig-core/log}
	 */
	get log() {
		return storage.log || (storage.log=require("./log"));
	},
	/**
	 * @type {module:pig-core/mutation}
	 */
	get mutation() {
		return storage.mutation || (storage.mutation=require("./mutation"));
	},
	/**
	 * @type {module:pig-core/notification}
	 */
	get notification() {
		return storage.notification || (storage.notification=require("./notification"));
	},
	/**
	 * @type {module:pig-core/promise}
	 */
	get promise() {
		return storage.promise || (storage.promise=require("./promise"));
	},
	/**
	 * @type {module:pig-core/proxy}
	 */
	get proxy() {
		return storage.proxy || (storage.proxy=require("./proxy"));
	},
	/**
	 * @type {module:pig-core/template}
	 */
	get template() {
		return storage.template || (storage.template=require("./template"));
	},
	/**
	 * @type {module:pig-core/urn}
	 */
	get urn() {
		return storage.urn || (storage.urn=require("./urn"));
	},
	/**
	 * @type {module:pig-core/util}
	 */
	get util() {
		return storage.util || (storage.util=require("./util"));
	},
	/**
	 * @type {module:pig-core/validator}
	 */
	get validator() {
		return storage.validator || (storage.validator=require("./validator"));
	}
};
